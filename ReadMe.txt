Hi this is my public repository. It includes some class assignments and personal projects that I'm working on.

Please turn on Word Wrap!

If there are any questions about portions of my code or projects send me an email: (conphil@okstate.edu)

~1~ Advanced Polymath - [Status - Completed {Personal Project}

This was a project I worked on for Dr. Ford Versypt with the purpose of:
(1) Practicing graphical user interface design and construction in MATLAB,
(2) to verify code implementation, and 
(3) to review content convered throughout the entire course. 

This project was to create and advanced version of the system of equations tool in Polymath.

Polymath is limited because it only allows a certain number of equations of each type. For a realistic non-homework program, the maximum number of equations is easily reached in Polymath.

My solution allows a "limitless" number of differential and algebraic equations.

If you are wanting to play around with the finished product I recommend installing the "MATLAB App Installer" file into your MATLAB. The code is also included for each of the GUI interfaces if you are curious how I've set up information to be solved/passed from userform to userform. There is a LaTex file used to create the PDF "verification" which displays the outputs you would expect to see when using the MATLAB Package!
]

~2~ Phillips Design - [Status - Complete {School Report}

Phillips 66 tasked my team with coming up with a solution for a problem they had with expanding the capacity of one of their plants. This involved research and computation that was mainly done through VBA. The computation and analysis was condensed into this LaTeX report.

We placed 2nd out of 14 teams and claimed a cash prize. 
]
~~ Snook - [Status - Research {Personal Project}

Recently began working on building a snooker table in the basement of my parents house. This game consists of keeping track of two persons scores and I was interesting in building some sort of calculator that is recessed into the side of the table. I'm wanting this controller to allow either player to add to their scores and enforce rules when necessary.

This will introduce me to the GUI package in python.

I'm hoping to then convert this GUI into a recessed Raspberry Pi controller or possibly an iphone app.

]

~~ Flight Request - [Status - Idle {Job Request}
]

~~ Goose.io - [Status - Idle {Start-up}
Hunters can pay to have their fields tracked by Goose.io. A hunter will lock into an annual contract, an employee will be sent to the location and drones will be used to record images of waterfowl on a hunters field/lake. The images that these take will be converted into positional data thats connected to an app. 
]

~~ Pool Path - [Status - Idle {Personal Project}
Using pathfindin and some sort of physics i would like to design a pool game that allows you to choose the ball you want to pot and the location where the ball will end up. The game should calculate the best possible sequence of collisions, the spin put onto the ball and the force at which it is hit to return the ball to said location. This would be a huge mix of logic and hopefully a challenging project.
]

~~ Resume - [Status - Idle {Personal Project}
I want to use LaTeX to create my resume rather than Microsoft Word
]

~~ Connect4 - [Status - Idle {Personal Project}
I want to build an AI to beat a player in Connect4. This project will help me understand applications of recursion using the minimax function approach as well as how to build game boards through web browsers. I will be using the approach provided by : Coding Train Tic Tac Toe.
]

~~ AdAstral - [Status - Idle {Start-up}

In collaboration with Mr. Bergkamp an initiative is being created to visualize payback periods for home/comercial solar panel arrays. 

Preliminary stages will involve collecting energy data from solar panels by area code. This energy production will be compared to a customers current energy costs and discount rate. I will be using solar panel index costs to do estimations until a buisness plan can come through to determine actual costs of installing these panels for customers by zip code. I'm wanting to show net present value diagrams and offer a pyaback period. Even if these customers never have a break even point I'm hoping that they will be able to see it's comparable to their current investments with low risk. 

I want this project to teach me about building an effective database!

Refrence - https://www.nrel.gov/docs/fy19osti/72399.pdf

I would also like to look into the effects of solar panels on realestate costs. If installing them raises the price of your home by the amount spent how could it not be worth installing?

Refrence - https://solaray.com.au/10-reasons-why-you-shouldnt-buy-solar-power/
]