\babel@toc {english}{}
\contentsline {section}{\numberline {1}EXECUTIVE SUMMARY}{I}% 
\contentsline {section}{\numberline {2}TABLE OF CONTENTS}{II}% 
\contentsline {section}{\numberline {A}INTRODUCTION}{1}% 
\contentsline {section}{\numberline {B}DESIGN BASIS}{2}% 
\contentsline {section}{\numberline {C}TECHNICAL DISCUSSION}{5}% 
\contentsline {subsection}{\numberline {C.a}\textit {Design Philosophy}}{5}% 
\contentsline {subsection}{\numberline {C.b}\textit {Description of the Process}}{6}% 
\contentsline {subsection}{\numberline {C.c}\textit {Technical Issues and Design Practices}}{14}% 
\contentsline {subsection}{\numberline {C.d}\textit {Safety Concerns}}{20}% 
\contentsline {subsection}{\numberline {C.e}\textit {Environmental Concerns}}{20}% 
\contentsline {section}{\numberline {D}ECONOMIC ANALYSIS}{20}% 
\contentsline {subsection}{\numberline {D.a}Revenue and Utility Costs}{20}% 
\contentsline {subsubsection}{\numberline {D.a.1}\textit {Raw Materials}}{20}% 
\contentsline {subsubsection}{\numberline {D.a.2}\textit {Ponca City, OK Weather}}{22}% 
\contentsline {subsubsection}{\numberline {D.a.3}\textit {Utility costs}}{23}% 
\contentsline {subsection}{\numberline {D.b}\textit {Capital Cost Estimates}}{25}% 
\contentsline {subsection}{\numberline {D.c}\textit {NPV Analysis}}{27}% 
\contentsline {subsubsection}{\numberline {D.c.1}Cumulative NPV}{27}% 
\contentsline {subsubsection}{\numberline {D.c.2}Comparing Options}{30}% 
\contentsline {subsection}{\numberline {D.d}\textit {Sensitivity Analysis}}{30}% 
\contentsline {section}{\numberline {E}CONCLUSIONS}{31}% 
\contentsline {section}{\numberline {F}RECOMMENDATIONS}{31}% 
\contentsline {section}{\numberline {A}Appendix: Figures not included in text}{34}% 
\contentsline {section}{\numberline {B}Appendix: Sizing Calculations}{36}% 
\contentsline {section}{\numberline {C}Appendix: Utility Calculations}{50}% 
\contentsline {section}{\numberline {D}Aspen Data}{53}% 
