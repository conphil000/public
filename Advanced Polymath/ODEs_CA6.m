%% Purpose:
% The purpose of this function is to solve a system of differential
% equations that a user inputs.
% These inputs will ahve strange syntax so there is some black magic we
% must participate in to solve complex issues.
%
%
% Author: Conor Phillips
%
% Date: 12/7/2019

% This should decipher all of these variables
% x_data is the integration range from x1 to x2 or 0 to 60 etc, linspace
% y0 is the initial values, their index should correspond to eqn index, i
    % may have to bring a toc over for analysis.
% c0 is the constants that the user provided,
% f0 is the differential equations that the user provided
function [output] = ODEs_CA6(X ,Y, c0, f0)
 % loack current values for y_values
    for j = 1:size(f0,1)
        eval([char(f0(j,3)),'=',char(string(Y(j))), ';']);
    end
    % load constants into function
    for j = 1:length(c0)
        eval([char(c0(j,1)),'=',char(c0(j,2)), ';']);
    end
    % evaluate the differential equations provided
    for j = 1:size(f0,1)
        eval(['out(j,1)=', char(f0(j,2)), ';'])
    end
    output = out;
 end


