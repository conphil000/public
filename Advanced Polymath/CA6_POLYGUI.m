% This userform will act as the storage for all data in system of
% equations. The other two userforms will add to this one directly and take
% information from the main gui as they are opened.

% To let these userforms bounce information to one another I'm changing
% handle visibility to on and i am setting their handles to names more
% intuitive like 'main', 'dif', and 'alg'. 

function varargout = CA6_POLYGUI(varargin)
% CA6_POLYGUI MATLAB code for CA6_POLYGUI.fig
%      CA6_POLYGUI, by itself, creates a new CA6_POLYGUI or raises the existing
%      singleton*.
%
%      H = CA6_POLYGUI returns the handle to a new CA6_POLYGUI or the handle to
%      the existing singleton*.
%
%      CA6_POLYGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CA6_POLYGUI.M with the given input arguments.
%
%      CA6_POLYGUI('Property','Value',...) creates a new CA6_POLYGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CA6_POLYGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CA6_POLYGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CA6_POLYGUI

% Last Modified by GUIDE v2.5 11-Dec-2019 20:58:04

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CA6_POLYGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @CA6_POLYGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- This will run as the Userform (main) is opening.
function CA6_POLYGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CA6_POLYGUI (see VARARGIN)

% Choose default command line output for CA6_POLYGUI
handles.output = hObject;

% Here i am opening cells to store data because i am used to python.
% I am storing all differential equations in dif, all algebraic in alg, and
% all initial values in init. I think they will need a range to integrate
% so i am adding that as band.
handles.dt = {};

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CA6_POLYGUI wait for user response (see UIRESUME)
% uiwait(handles.main);


% --- Outputs from this function are returned to the command line.
function varargout = CA6_POLYGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- This button adds a differential equation and its initial value
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton4,'Visible','on')
set(handles.pushbutton5,'Visible','on')
set(handles.pushbutton17,'Visible','on')
CA6_DIFGUI

% --- This button adds an algebraic equation
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton5,'Visible','on')
set(handles.pushbutton8,'Visible','on')
set(handles.pushbutton19,'Visible','on')
CA6_ALGGUI


% --- this button should evaluate the system of equations
function pushbutton3_Callback(hObject, eventdata, handles)
set(handles.pushbutton15,'Visible','on')
set(handles.pushbutton14,'Visible','on')

% How could we solve with so many equations?
% I'm thinking i will build a data table that has [[val, name], [val,
% name]] so when i look up a name the value is associated with it? lets
% try.
% 1st -- I want to grab everything out of their corresponding areas
    list1 = get(handles.listbox1,'String');
    list2 = get(handles.listbox2,'String');
    list3 = get(handles.listbox3,'String');
    list4 = get(handles.text5,'String');
    
    if class(list1) == 'cell' & class(list2) == 'cell' & class(list3) == 'cell'
        % I'm storign ODE in list1, IV in list2, alg eqn in list3, and
        % integration limits in list 4.
        l1 = max(size(list1));
        l2 = max(size(list2));
        l3 = max(size(list3));
        % Pre-Allocating space for these variable and values
        data1 = strings(l1,2);
        data2 = strings(l2,2);
        data3 = strings(l3,2);
        data4 = strings(1,2);
        df = {data1,data2,data3,data4};
%  2nd -- Lets try adding values into these spaces
        % going to look through the list and split into halves then load
        % halves!
        %Looking through list1!!
        %%%%%% Multi Level Indexing{cell,cell}(data,data)%%%%%
        %% This is not useful after learning eval, will still keep
        x = {list1,list2,list3};
        index = 0;
        for data = x
            L = cellfun('prodofsize', data); %this find the number of cells?
            index = index + 1;
            load = df(index);
            for i = 1:L
                temp = split(string(data{1,1}(i))); %split at the spaces, we can pull later.
                load(i,1) = {temp(1)};
                load(i,2) = {temp(3)}; 
            end
            % I'm too good at this.
            df(index) = {string(load)};
        end
        % For my next trick i shall find the comma inside of this list 4
        % string, it looks like this (value, value2). it will always start
        % from second character so if i find location of comma -1 i should
        % have first value.
        comma = strfind(list4,',');
        value1 = list4(2:comma-1);
        value2 = list4(comma+1:end-1);
        if value2 == 0
            f = msgbox('Please enter integration limits.');
            return
        end
        % wow first try
        temp = {value1 , value2};
        %load into dataframe
        % easier way is to say dt{1,1}(4) = temp
        df(4) = {temp};
        
        % All of the values the user provided should now be stored in dt.
        %dt(1) is dif, dt(2) is IV, dt(3) is alg, dt(4) is integration.
        % if i wanted inital value for integration i would say dt{1,4}(1,1)
% 3rd -- Now how do i replace strings with their algebra?
        % some algebraic equations use other constants so i probably want
        % to look through them twice? i need to look at last assignment to
        % see how we solved ODE45.
        %My interpretation of ODE45 requirements:
            % Need an Initial Time
            % Need a range of X values
            % Initial values stored into an array
            % ydata? This is needed if we were solving for some constant i
            % guess
            % then all parameters stored as an array
            dt = {};
        % Plan: Store these values how they should be stored, go through
        % all equations and replace them with the c(1) etc? 
        % I would call this dynamic programming approach?
% 4th -- Let's create a data set of x-intercept values!
        
        x_o = str2double(df{1,4}(1,1));
        x_f = str2double(df{1,4}(1,2));
        someconstant = x_f/100;
        if isnan(x_f)
            f = msgbox("Please don't enter fractions for integration limits.")
        else
            x_data = x_o:someconstant:x_f;
        %x-intercept values created
% 5th -- Initial Values into an array for understandability, going to refer
        % to it as y data
            y0 = str2double(df{1,2}(:,2));
% 6th -- Parameters should be stored, going to refer to them as c0
            c0 = df{1,3};
% 7th -- recoup
        % this is what ode45 will end up looking like
        % what i have: V, initial values, constants
        % what i dont have:EQUATIONNS???
        
        %[V, F_and_T] = ode45(@ODEs_CA3, V, [initial_Fa_dV initial_Fb_dV 
        %initial_Fc_dV initial_T_dV]);
% 8th -- Equations i shall refer to these as f0
            f0 = df{1,1};
            %I am going to comma seperate every constant!
            % I'm doing this so i can create @(k1,k2,k3)k1*k2*k3 and its
            % scalable!
            
            var = c0(:,1);
            var = sprintf('%s,',var);
            var = var(1:end-1);
            var = ['(' char(var) ')'];
            for i = 1:length(f0)
                slash = strfind(f0(i,1),'/');
                value1 = f0{i,1}(1:slash-1);
                value2 = f0{i,1}(slash+1:end);
                f0(i,1) = [value1,'_',value2];
                f0(i,3) = [value1(2:end)];
                f0(i,4) = [value2(2:end)];
            end
            df{1,1} = f0;
            %easiest way i found to get arguments for each function
%             numfun = length(fx0);
%             args = {};
%             for i = 1:numfun
%                 args(end+1) = {argnames(fx0{1,i})};
%                 temp = string(args{:,i});
%                 temp = char(sprintf("%s,",temp));
%                 temp = temp(1:end-1);
%             end
%             fx0 = {};
%             for i = 1:numfun
%                 temp = string(args{:,i});
%                 temp = char(sprintf("%s,",temp));
%                 temp = ['@(' temp(1:end-1) ')'];
%                 fx0(end+1) = {str2func([temp char(f0(i,2))])};
%             end
%             % Functions are stored in fx0 thanks to Black Magic!!!
%             % how on earth do i find these values???
%               % pull initial values from y0
            init = [];
            for i = 1:length(y0)
                eval(['init(i)=',char(num2str(y0(i))), ';']);
            end
            handles.X = x_data;
            func = @(X, Y) ODEs_CA6(X,Y,c0,f0);
            [handles.X, handles.Y] = ode45(func, handles.X, init, c0);
            
            handles.dt = df;
            if ~isempty(handles.Y)
                pushbutton12_Callback(hObject, eventdata, handles)
                f = msgbox('Evaluation Complete.');
            end
            
        end
    else
        f = msgbox('Please provide all variables required for evaluation!');
    end
guidata(hObject, handles);
    
    
% --- This button will plot all the values
function pushbutton12_Callback(hObject, eventdata, handles)
f0 = handles.dt{1};
set(handles.edit1,'String',strjoin(f0(:,3),','))
pushbutton13_Callback(hObject, eventdata, handles)

% --- updates graph with modified subset
function pushbutton13_Callback(hObject, eventdata, handles)
f0 = handles.dt{1};
want = split(get(handles.edit1,'String'),',');
axes(handles.axes2);
cla

for i = 1:size(want,1)
    v1 = string(want{i});
    for j = 1:size(f0,1)
         v2 = string(f0(j,3));
         if v1==v2
             plot(handles.X,handles.Y(:,j))
             grid on
             hold on
             leg{i} = v2;
         end
     end
end
xlabel(['Independent Variables - ' char(f0{1,4})]);
ylabel(['Dependent Variables - ' char(f0{1,3}(1))]);
 legend(leg)
 hold off




% --- This button will remove selected ODE and its Initial Value
function pushbutton4_Callback(hObject, eventdata, handles)
% Going to remove an item from diff list
% 1st -- grab items in both lists
list1 = get(handles.listbox1,'String');
list2 = get(handles.listbox2,'String');
items = max(size(list1));
if class(list1) == 'char'
    new_val1 = 'Empty';
    new_val2 = 'Empty';
else
    val = get(handles.listbox1,'Value');
    set(handles.listbox1,'Value',1);
    set(handles.listbox2,'Value',1);
    list1(val) = [];
    list2(val) = [];
    new_val1 = list1;
    new_val2 = list2;
    if items == 1
        new_val1 = 'Empty';
        new_val2 = 'Empty';
    end
end
set(handles.listbox1,'String',new_val1);
set(handles.listbox2,'String',new_val2);


% --- This button will clear all of the lists
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.listbox1,'String','Empty')
set(handles.listbox2,'String','Empty')
set(handles.listbox3,'String','Empty')
set(handles.listbox1,'Value',1)
set(handles.listbox2,'Value',1)
set(handles.listbox3,'Value',1)
set(handles.text5,'String', '(0, 0)')


% --- This button removes an item from algebraic list.
function pushbutton8_Callback(hObject, eventdata, handles)
% Going to remove an item from diff list
% 1st -- grab items in both lists
list1 = get(handles.listbox3,'String');
items = max(size(list1));
if class(list1) == 'char'
    new_val1 = 'Empty';
else
    val = get(handles.listbox3,'Value');
    set(handles.listbox3,'Value',1);
    list1(val) = [];
    new_val1 = list1;
    if items == 1
        new_val1 = 'Empty';
    end
end
set(handles.listbox3,'String',new_val1);

% --- This button sets the integration limits
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton3,'Visible','on')
CA6_INTGUI
% --- Export CSV
function pushbutton15_Callback(hObject, eventdata, handles)
input1 = char(inputdlg('Please enter a path for this file.'));
input2 = char(inputdlg('Please enter a name for this file.'));
name = ['\' input2 '.csv'];
path = [input1];
tot = [path name];
data = [handles.X handles.Y];
csvwrite(tot,data)


% --- Export Graph
function pushbutton14_Callback(hObject, eventdata, handles)
input1 = char(inputdlg('Please enter a path for this file.'));
input2 = char(inputdlg('Please enter a name for this figure.'));
name = ['\' input2 '.png'];
path = [input1];
tot = [path name];
leg = legend(handles.axes2);
fignew = figure('Visible','off'); % Invisible figure
newAxes = copyobj(handles.axes2,fignew);
legend(leg.String)
set(handles.axes2,'units','normalized');
set(fignew, 'PaperPositionMode', 'auto');
set(newAxes,'Position',get(groot,'DefaultAxesPosition'));
print(fignew,tot,'-dpng')

% --- edit an item in ODE
function pushbutton17_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    set(handles.edit3,'String','')
    set(handles.edit6,'String','')
set(handles.uipanel12,'Visible','on')
set(handles.edit6,'Visible','on')
val = get(handles.listbox1,'Value');
list = get(handles.listbox1,'String');
list1 = get(handles.listbox2,'String');
set(handles.edit3,'String',list{val});
set(handles.edit6,'String',list1{val});
% list{val} = [];
% list1{val} = [];
% set(handles.listbox1,'String',list)
% set(handles.listbox2,'String',list1)

% --- edit an item in alg
function pushbutton19_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    set(handles.edit3,'String','')
    set(handles.edit6,'String','')
set(handles.uipanel12,'Visible','on')
set(handles.edit6,'Visible','off')
val = get(handles.listbox3,'Value');
list = get(handles.listbox3,'String');
set(handles.edit3,'String',list{val});
% list{val} = [];
% set(handles.listbox3,'String',list)


% --- Executes on button press in pushbutton20.
function pushbutton20_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
edit1 = get(handles.edit3,'String');
edit2 = get(handles.edit6,'String');
if ~isempty(edit2)
    val = get(handles.listbox1,'Value');
    list = get(handles.listbox1,'String');
    list1 = get(handles.listbox2,'String');
    list{val} = edit1;
    list1{val} = edit2;
    set(handles.listbox1,'String',list)
    set(handles.listbox2,'String',list1)
    set(handles.edit3,'String','')
    set(handles.edit6,'String','')
else
    val = get(handles.listbox3,'Value');
    list = get(handles.listbox3,'String');
    list{val} = edit1;
    set(handles.listbox3,'String',list)
    set(handles.edit3,'String','')
end
set(handles.uipanel12,'Visible','off')
% ---Closes all
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close all;

% --- Wont be using.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.listbox2,'Value',get(handles.listbox1,'value'));
% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1

% --- Wont be using.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Wont be using.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.listbox1,'Value',get(handles.listbox2,'value'));
% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Wont be using.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Wont be using.
function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox3


% --- Wont be using.
function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end





function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


