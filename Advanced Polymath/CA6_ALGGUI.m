% This userform will open when called from main form, its purpose is to add
% an algebraic equation into the algebraic listbox. At this point i am not
% checking if their input is valid because the user should only input valid
% numbers.

% To let these userforms bounce information to one another I'm changing
% handle visibility to on and i am setting their handles to names more
% intuitive like 'main', 'dif', and 'alg'. 

function varargout = CA6_ALGGUI(varargin)
% CA6_ALGGUI MATLAB code for CA6_ALGGUI.fig
%      CA6_ALGGUI, by itself, creates a new CA6_ALGGUI or raises the existing
%      singleton*.
%
%      H = CA6_ALGGUI returns the handle to a new CA6_ALGGUI or the handle to
%      the existing singleton*.
%
%      CA6_ALGGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CA6_ALGGUI.M with the given input arguments.
%
%      CA6_ALGGUI('Property','Value',...) creates a new CA6_ALGGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before CA6_ALGGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to CA6_ALGGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CA6_ALGGUI

% Last Modified by GUIDE v2.5 07-Dec-2019 13:17:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CA6_ALGGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @CA6_ALGGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CA6_ALGGUI is made visible.
function CA6_ALGGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to CA6_ALGGUI (see VARARGIN)

% Choose default command line output for CA6_ALGGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes CA6_ALGGUI wait for user response (see UIRESUME)
% uiwait(handles.alg);


% --- Outputs from this function are returned to the command line.
function varargout = CA6_ALGGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- This button will add items to listbox on main.
function pushbutton1_Callback(hObject, eventdata, handles)
% Lets add an item to a listbox on GUI 'main'.
% 1st -- We need to find the GUI 'main'. We will store the objects in main.
    main = findobj('tag', 'main');
    % This will store the data from main into data_main.
    % data_main will be like handles for GUI 'main'
    if ~isempty(main)
        main_data = guidata(main);
    end
% 2nd -- We need to create the string we want to add to main.
    a = get(handles.edit1,'String');
    b = get(handles.edit2,'String');
    if ~isempty(a) & ~isempty(b)
        ab = [a, ' = ', b];
% 3rd -- We need to know what values are currently in the listbox.
        old = get(main_data.listbox3, 'String');
        % Listbox will only hold cells, so if there is a 'char' data type in it that
        % means nothing has been added yeat and we need to create a new cell.
        if class(old) == 'char'
            old = {};
        end
% 4th -- Add the new item to the end of the current items.
        old{end+1} = ab;
        new = old;
% 5th -- Load the updated item list to GUI 'main'
        set(main_data.listbox3,'String',new);
% 6th -- Close the current window by running code that runs when you hit
% close.
        pushbutton3_Callback
    else
        % You didn't insert valid information.
        f = msgbox('Please use valid inputs.');
    end 
% This works correctly.
 



% --- This button will clear current values in both textboxes.
function pushbutton2_Callback(hObject, eventdata, handles)

set(handles.edit1,'String','');
set(handles.edit2,'String','');

% --- This button will close the alg userform
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close CA6_ALGGUI




% Will not be used.
function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Will not be used.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% Will not be used.
function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Will not be used.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
